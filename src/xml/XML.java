package xml;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Vertice;
import modelo.Aresta;
import modelo.Grafo;

public class XML {

    static Scanner ler = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println("## XML ##");
        while (true) {
            int resp = menu();
            if (resp == 1) {
                Vertice vertices = new Vertice();
                System.out.print("Número de vértices: ");
                vertices.setVertice(ler.nextInt());

                XStream xml = new XStream(new DomDriver());
                xml.alias("vértice", Vertice.class);
                xml.alias("aresta", Aresta.class);

                //System.out.println("xml ==> "+xml.toXML(pes));
                File xmlFile = new File(g.nome + ".xml");
                try {
                    xml.toXML(vertices, new FileWriter(xmlFile));
                    System.out.println("Vértice Criado");
                } catch (IOException ex) {
                    System.out.println("Erro ao criar o vértice");
                }
            } else if (resp == 2) {
                XStream xml = new XStream(new DomDriver());
                xml.alias("vértice", Vertice.class);
                xml.alias("aresta", Aresta.class);

                File xmlFile = new File("grafo.xml");
                Grafo g = (Grafo) xml.fromXML(xmlFile);
                System.out.println("==> " + g.getNome());
            } else if (resp == 3) {
                break;
            }

        }
        System.out.println("## Até Logo ##");

    }

    private static int menu() {
        System.out.println("# 1) Número de vértices: ");
        System.out.println("# 2) Números de arestas: ");
        System.out.println("# 3) Sair");
        int resp = ler.nextInt();
        return resp;
    }

}
